#ifndef WEKTOR_HH
#define WEKTOR_HH

#include "rozmiar.h"
#include <iostream>


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Wektor {
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
double tab[ROZMIAR];
  public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */ 
double daj(int b) const { return tab[b]; }
void wez(double z, int p) { tab[p]=z; }
double  operator [] (unsigned int Ind) const { return tab[Ind]; }
double &operator [] (unsigned int Ind)       { return tab[Ind]; }
double dlwekbl();
};


std::istream& operator >> (std::istream &Strm, Wektor &Wek);

std::ostream& operator << (std::ostream &Strm, const Wektor &Wek);

Wektor operator + (Wektor Zm1, Wektor Zm2);

Wektor operator - (Wektor Zm1, Wektor Zm2);

double operator * (Wektor Zm1, Wektor Zm2);

Wektor operator * (Wektor Zm1, double Zm2);

Wektor operator / (Wektor Zm1, double Zm2);

#endif
