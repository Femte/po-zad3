#include "Macierz.hh"


/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Macierz, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

std::ostream& operator << (std::ostream &Strm, const Macierz &Mac)
{
    Strm << std::endl;
    for(int i = 0; i<ROZMIAR; ++i) 
    {
        Strm << "\t" << Mac.dajw(i) << std::endl;
    }
    return Strm;
}

std::istream& operator >> (std::istream &Strm, Macierz &Mac)
{
    Wektor wek;
    for(int i = 0; i<ROZMIAR; ++i) 
    {
        Strm >> wek;
        Mac.wezw(wek,i); 
    }
    return Strm;
}

void Macierz::transponuj()
{
    Wektor C, D;
    Macierz mac;

    for(int i=0; i<ROZMIAR; ++i)
    {
        for(int y=0; y<ROZMIAR; ++y) 
        {                           
            C = tab[y];
            D[y] = C[i];
        }
        mac.wezw(D, i); 
    }

    for(int b = 0; b<ROZMIAR; ++b)
    {
        tab[b] = mac.dajw(b); 
    }
}

double Macierz::det()
{
    Macierz mac = (*this);
    double elem = 0, wynik = 1;

    for (int i = 0; i<ROZMIAR-1; i++)
    {
        for (int j = i+1; j<ROZMIAR; j++)
        {
            elem = -mac.tab[j][i]/mac.tab[i][i];
            for (int k = i; k<=ROZMIAR; k++)
            {
                mac.tab[j][k]+=(elem*mac.tab[i][k]);
            }
        }
    }

    for (int l = 0; l<ROZMIAR; l++)
    {
        wynik = (mac(l, l) * wynik);

    }
    return wynik;
}

Macierz::Macierz(int a)
{
    int c = 0;

    for(int i=0; i<ROZMIAR; ++i) 
    {
        for(int j=0; j<ROZMIAR; ++j)
        {
            tab[i][j]=0;
        }
    }

    for(int b=0; b<ROZMIAR; ++b) 
    {
        tab[c][b]=a;
        c++;
    }
}
