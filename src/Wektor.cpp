#include "Wektor.hh"


/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Wektor, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

std::istream& operator >> (std::istream &Strm, Wektor &Wek)
{
    double war;
    for(int i=0; i<ROZMIAR; ++i)
    {
        Strm >> war;
        Wek.wez(war,i); 
    }
    return Strm;
}

std::ostream& operator << (std::ostream &Strm, const Wektor &Wek)
{
    for(int i=0; i<ROZMIAR; ++i) 
    {
        Strm << Wek.daj(i) << "  "; 
    }
    return Strm;
}

Wektor operator + (Wektor Zm1, Wektor Zm2)
{
    Wektor wek;
    double wynik;
    for (int i=0; i<ROZMIAR; ++i) 
    {
        wynik = Zm1.daj(i) + Zm2.daj(i); 
        wek.wez(wynik,i); 
    }
    return wek;
}

Wektor operator - (Wektor Zm1, Wektor Zm2)
{
    Wektor wek;
    double wynik;
    for (int i=0; i<ROZMIAR; ++i)
    {
        wynik = Zm1.daj(i) - Zm2.daj(i);
        wek.wez(wynik,i);
    }
    return wek;
}

double operator * (Wektor Zm1, Wektor Zm2)
{
    double iloczyn, wynik;
    for (int i=0; i<ROZMIAR; ++i)
    {
        iloczyn = Zm1.daj(i) * Zm2.daj(i);
        wynik+=iloczyn; 
    }
    return wynik;
}

Wektor operator * (Wektor Zm1, double Zm2)
{
    Wektor wek;
    double wynik;
    for (int i=0; i<ROZMIAR; ++i)
    {
        wynik = Zm1.daj(i) * Zm2;
        wek.wez(wynik, i);

    }
    return wek;
}

Wektor operator / (Wektor Zm1, double Zm2)
{
    Wektor wek;
    double wynik;
    for (int i=0; i<ROZMIAR; ++i)
    {
        wynik = Zm1.daj(i) / Zm2;
        wek.wez(wynik, i);

    }
    return wek;
}

double Wektor::dlwekbl()
{
    double C = 0;
    for(int i = 0; i<ROZMIAR; ++i) 
    {
        C += pow(tab[i], 2);
    }
    return sqrt(C);
}


