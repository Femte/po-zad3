#include "UkladRownanLiniowych.hh"


/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy UkladRownanLiniowych, ktore zawieraja 
 *  wiecej kodu niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

std::istream& operator >> (std::istream &Strm, UkladRownanLiniowych &UklRown)
{
    Wektor wek;
    double liczba;
    for(int i=0; i<ROZMIAR; ++i) 
    {
        Strm >> wek;
        UklRown.wezmU(wek, i);
    }

    for(int i=0; i<ROZMIAR; ++i) 
    {
        Strm >> liczba;
        UklRown.wezwU(liczba, i);

    }

    return Strm;
}

std::ostream& operator << (std::ostream &Strm, const UkladRownanLiniowych &UklRown)
{
    Macierz W;
    Strm << std::endl << "Uklad rownan do rozwiazania: " << std::endl << std::endl;
    for(int i=0; i<ROZMIAR; ++i) 
    {
        Strm << "|  " << UklRown.dajj(i) <<  "| | x_" << i+1 << " |";
        if((i==ROZMIAR/2)) 
        {
            Strm << " = | " << UklRown.dajwU(i) << " |" << std::endl;
        }
        else
        {
            Strm << "   | " << UklRown.dajwU(i) << " |" << std::endl;
        }

    }
    Strm << std::endl << std::endl;
    return Strm;
}

Wektor UkladRownanLiniowych::obliczuklad ()
{
    double x, y;
    Wektor wynik, tmpglowny;
    Macierz X;

    X = mac; 
    y = X.det(); 
    for (int i = 0; i<ROZMIAR; ++i)
    {
        tmpglowny = mac.dajw(i);
        mac.wezw(wek, i); 
        X = mac; 
        x = X.det(); 
        wynik.wez(x/y, i); 
        mac.wezw(tmpglowny, i); 
    }

    return wynik; 
}


Wektor UkladRownanLiniowych::wekbl(Wektor zz)
{
    float b = 0;
    Wektor X;
    Macierz B = mac;

    for(int i = 0; i<ROZMIAR; ++i) 
    {
        for(int j = 0; j<ROZMIAR; ++j)
        {
            b += (B(i,j)*zz[j]); 
        }
        X[i] = b; 
        b = 0;
    }
    return X - wek; 
}

void wyswrozw(Wektor C, Wektor B, double x)
{
  std::cout << "Wektor rozwiazan:" << std::endl  << std::endl << "\t     " << C << std::endl << std::endl;
  std::cout << "Wektor bledu:" << std::endl << std::endl << "\tAx-b  =  (  " << B << ")" << std::endl << std::endl;
  std::cout << "Dlugosc wektora bledu:" << std::endl << std::endl << "\t||Ax-b||  =  " << x << std::endl << std::endl;
}

