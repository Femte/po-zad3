#include <iostream>
#include "Wektor.hh"
#include "Macierz.hh"
#include "UkladRownanLiniowych.hh"



using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */


int main()
{
  UkladRownanLiniowych B;
  Wektor C, D;
  double x;

  cin >> B;

  C = B.obliczuklad(); 
  B.wywtrans(); 

  cout << B;

  D = B.wekbl(C); 
  x = D.dlwekbl(); 
  wyswrozw(C, D, x); 
}
